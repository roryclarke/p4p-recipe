#!/bin/bash

# Set location of EPICS modules
cat <<EOF > configure/RELEASE.local
EPICS_BASE=${EPICS_BASE}
LIB_SYS_LIBS += pvxs
EOF

make distclean
make

# Install files under PREFIX
mv python*/${EPICS_HOST_ARCH}/p4p ${SP_DIR}/
# We let conda create the pvagw entrypoint
mv bin/${EPICS_HOST_ARCH}/pvagw ${PREFIX}/bin/

